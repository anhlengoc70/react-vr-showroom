import * as THREE from "three";
import React, { useEffect } from "react";

var container, windowHalfX, windowHalfY;
var camera, scene, renderer, spotLight, dirLight;

var video;
export default function VideoPlayer() {
  useEffect(() => {
    video = document.getElementById("video");
      init();
      animate();
  }, []);

  function init() {
    container = document.getElementById("container");
    document.body.appendChild(container);
    camera = new THREE.PerspectiveCamera(
      45,
      window.innerWidth / window.innerHeight,
      1,
      1000
    );
    camera.position.set(0, 10, 35);

    scene = new THREE.Scene();

    // Lights

    scene.add(new THREE.AmbientLight(0x404040));

    spotLight = new THREE.SpotLight(0xffffff);
    spotLight.position.set(10, 10, 5);
    scene.add(spotLight);

    dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
    dirLight.position.set( 0.5, 1, 1 ).normalize();
    scene.add(dirLight);

    var texture = new THREE.VideoTexture(video);

    var geometry = new THREE.PlaneBufferGeometry(16, 10, 50, 20);
    var material = new THREE.MeshBasicMaterial({
      map: texture,
      side: THREE.DoubleSide,
    });
      var videoPlay = new THREE.Mesh(geometry, material);
      videoPlay.position.set(0, 6, 0);
      videoPlay.castShadow = true;
      videoPlay.receiveShadow = true;
      scene.add(videoPlay);

      var groundgeometry = new THREE.BoxBufferGeometry(10 , 0.2, 5);
      var material = new THREE.MeshPhongMaterial({
        color: 0xa0adaf,
        shininess: 150,
        specular: 0x111111,
      });
  
      var ground = new THREE.Mesh(groundgeometry, material);
      ground.scale.multiplyScalar(3);
      ground.castShadow = false;
      ground.receiveShadow = true;
      scene.add(ground);
      
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    window.addEventListener("resize", onWindowResize, false);
  }

  function onWindowResize() {
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    camera.aspect = window.innerWidth / window.innerHeight;
    renderer.setSize(window.innerWidth, window.innerHeight);
  }

  function animate() {

    renderer.setAnimationLoop( render );

  }

  function render() {

    renderer.render( scene, camera );

  }

  return (
    <div>
      <div id="container"></div>
      <video
        id="video"
        loop
        crossOrigin="anonymous"
        playsInline
        autoPlay
        style={{ display: "none" }}
        src={require("../assets/video.mp4")}
      ></video>
    </div>
  );
}
