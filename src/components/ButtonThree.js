import React, { useEffect } from 'react';
import * as THREE from 'three';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { CSS3DObject } from "three/examples/jsm/renderers/CSS3DRenderer";

var audio = new Audio(require('../pages/media/menu-click.wav'));
var domObject;

export default function ButtonThree({title, posX, posY, posZ, rotate, imgUrl, onClick, onRenderButton})
{        
    useEffect(()=>{
        addButton(); 
        console.log("add button", domObject);

    })
    function onRender(){
        onRenderButton(domObject);
    }
    const addButton = () =>{
        console.log( title, posX, posY, posZ, rotate, imgUrl)
        var element = document.getElementById( 'button' );
        element.style.width = '50px';
        element.style.height = '20px';
        element.style.border = '1px solid orange';
        element.style.borderRadius = "3px";                      
        element.style.opacity = 0.99;

        
        element.style.backgroundImage = 'url('+ imgUrl +')';
        element.style.backgroundSize = "50px 20px";
        element.style.transition = "all 0.5s";
        element.addEventListener('click', () => buttonOnClick(title));
        element.addEventListener('mouseover',() => onMouseOver(element));
        element.addEventListener('mouseout',() => onMouseOut(element));
        domObject = new CSS3DObject( element );
        domObject.position.set( -200, posY, posZ);
        domObject.rotateY(1.7 + rotate);
        onRender();
    }
    function onMouseOver(element){
        element.style.width = 1.3 * 50 +'px';
        element.style.backgroundSize = 1.3*50 +"px 20px";
    }
    function onMouseOut(element){
        element.style.width = 50 +'px';
        element.style.backgroundSize = 50 +"px 20px";
    }
    function buttonOnClick (){
        onClick();
        audio.play();
    };
    return(
        <div>
            <div id = "button"></div>
        </div>
    )
}