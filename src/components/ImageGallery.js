import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";

var scenes;
function ImageGallery(scenes) {
  const classes = useStyles();

  const handleScrollLeft = () => {
    var container = document.getElementById("container");
    container.scrollLeft -= 100;
  };
  const handleScrollRight = () => {
    var container = document.getElementById("container");
    container.scrollLeft += 100;
  };

  useEffect(() => {
    console.log(scenes);
  }, []);
  return (
    <div
      style={{
        textAlign: "center",
        position: "absolute",
        bottom: 100,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        zIndex: 1010,
        width: "100%",
      }}
    >
      <div
        id="wrapper"
        style={{
          height: "100px",
          fontSize: "25px",
          marginLeft: "10px",
          marginRight: "10px",
          display: "flex",
          flexDirection: "row",
          lineHeight: "100px",
        }}
      >
        <a
          id="prev"
          style={{
            paddingTop: "10px",
            //paddingBottom: "10px",
            height: "100%",
            cursor: "pointer",
            color: "black",
            transition: "0.6s ease",
          }}
          onClick={handleScrollLeft}
        >
          &#10094;
        </a>
        <div
          id="container"
          style={{
            verticalAlign: "middle",
            display: "inline-block",
            whiteSpace: "nowrap",
            overflowX: "hidden",
            overflowY: "hidden",
            maxWidth: "600px",
          }}
        >
          <div id="image" className={classes.image}>
            1
          </div>
          <div id="image" className={classes.image}>
            2
          </div>
          <div id="image" className={classes.image}>
            3
          </div>
          <div id="image" className={classes.image}>
            4
          </div>
          <div id="image" className={classes.image}>
            5
          </div>
          <div id="image" className={classes.image}>
            6
          </div>
          <div id="image" className={classes.image}>
            7
          </div>
          <div id="image" className={classes.image}>
            8
          </div>
          <div id="image" className={classes.image}>
            9
          </div>
          <div id="image" className={classes.image}>
            10
          </div>
        </div>
        <a
          id="next"
          style={{
            paddingTop: "10px",
            paddingBottom: "10px",
            height: "100%",
            cursor: "pointer",
            color: "black",
            transition: "0.6s ease",
          }}
          onClick={handleScrollRight}
        >
          &#10095;
        </a>
      </div>
    </div>
  );
}
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    height: "100vh",
  },
  form: {
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
    [theme.breakpoints.up("md")]: {
      width: "78%",
    },
    [theme.breakpoints.up("lg")]: {
      width: "64%",
    },
    height: "80vh",
    margin: "auto",
  },
  image: {
    height: "100px",
    width: "100px",
    fontSize: "25px",
    background: "blue",
    marginLeft: "10px",
    marginRight: "10px",
    display: "inline-block",
    flexDirection: "row",
    lineHeight: "100px",
  },
}));
export default ImageGallery;
