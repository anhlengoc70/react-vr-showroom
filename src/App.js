import React, { useState, useEffect } from "react";
import "./App.css";
import Home from "./pages/Home";
import ShowRoom from "./pages/ShowRoom";
import SceneManager from "./pages/SceneManager";
import config from "./components/datas/ShowroomWithVR.json"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
function App() {
  const [isDetails, setIsDetails] = useState(false);
  const [isInTour, setIsInTour] = useState(false);
  const [maker, setMaker] = useState("");


  useEffect(() => {
  }, []);

  return (
    <Router>
        <Switch>
          <Route path="/VRShowroom">
            <SceneManager
              scenes={config.scenes}
              initialSceneId={config.initialSceneId}
              onBack={() => setIsInTour(false)}
            />
          </Route>
          <Route path="/showroom">
            <ShowRoom
                onBack={() => setIsDetails(false)}
                onClickVideo={() => { }}
                onClickTour={() => setIsInTour(true)}
              />
          </Route>
          <Route path="/">
            <Home
              onClick={(maker) => {
                setIsDetails(true);
                setMaker(maker);
              }}
            ></Home>
          </Route>
        </Switch>
    </Router>
  );
  //   <div className="App">
  //     {/* {isDetails ?
  //       isInTour ? ( */}
          
  //       {/* ) : (
  //           <ShowRoom
  //             onBack={() => setIsDetails(false)}
  //             onClickVideo={() => { }}
  //             onClickTour={() => setIsInTour(true)}
  //           />
  //         ) : (
  //         <Home
  //           onClick={(maker) => {
  //             setIsDetails(true);
  //             setMaker(maker);
  //           }}
  //         ></Home>
  //       )
  //     } */}
  //   </div>
  // );
}

export default App;
