import React, { useEffect, useState } from "react";
import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import {
  CSS3DObject,
  CSS3DRenderer,
} from "three/examples/jsm/renderers/CSS3DRenderer";
//import ImageGallery from "../components/ImageGallery";
import { Button, Box, SvgIcon } from "@material-ui/core";
//import { makeStyles } from "@material-ui/core/styles";

var camera, element, scene, renderer, renderer2, windowHalfX, windowHalfY;
var controls;
var loader, geometry;
var mouseX, mouseY;
var audio = new Audio(require("./media/menu-click.wav"));

function SceneManager({ initialSceneId, scenes, onBack }) {
  const [currentSceneId, setCurrentSceneId] = useState(initialSceneId);
  const [displayInA, setDisplayInA] = useState("On");
  const [displayInB, setDisplayInB] = useState("disable");
  const [displayInC, setDisplayInC] = useState("disable");
  //const classes = useStyles();

  var container, buttonrender, startButton, main, mesh, mesh1;
  windowHalfX = window.innerWidth / 2;
  windowHalfY = window.innerHeight / 2;
  const CAMERA_DEFAULT_Z = 0.01;
  const CAMERA_DEFAULT_FOV = 90;
  let scale = 0.5;

  const onChangePosition = (id) => {
    console.log(id);
    if (id == "hallA") {
      setDisplayInA("On");
      setDisplayInB("disable");
      setDisplayInC("disable");
    }
    if (id == "hallB") {
      setDisplayInA("disable");
      setDisplayInB("disable");
      setDisplayInC("On");
    }
    if (id == "lobby") {
      setDisplayInA("disable");
      setDisplayInB("On");
      setDisplayInC("disable");
    }
    console.log("ABC", displayInA, displayInB, displayInC);
  };

  useEffect(() => {
    console.log("height width", window.innerWidth, window.innerHeight);
    container = document.getElementById("homeBanner");
    buttonrender = document.getElementById("css");
    startButton = document.getElementById("start");
    main = document.getElementById("main");
    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xf0f0f0);

    camera = new THREE.PerspectiveCamera(
      CAMERA_DEFAULT_FOV,
      window.innerWidth / window.innerHeight,
      1,
      1100
    );
    camera.position.set(10, 0, CAMERA_DEFAULT_Z);

    loader = new THREE.TextureLoader();
    geometry = new THREE.SphereBufferGeometry(100, 400, 380);
    // invert the geometry on the x-axis so that all of the faces point inward
    geometry.scale(-1, 1, 1);
    // invert the geometry on the x-axis so that all of the faces point inward
    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth- 5, window.innerHeight -5);
    container.appendChild(renderer.domElement);

    renderer2 = new CSS3DRenderer();
    renderer2.setSize(window.innerWidth, window.innerHeight);
    renderer2.domElement.style.position = "absolute";
    renderer2.domElement.style.top = 0;
    buttonrender.appendChild(renderer2.domElement);

    var ambientLight = new THREE.AmbientLight(0xcccccc);
    scene.add(ambientLight);

    document.addEventListener("mousemove", onDocumentMouseMove, false);
    controls = new OrbitControls(camera, renderer2.domElement);
    controls.enableZoom = false;
    controls.enablePan = false;
    controls.enableDamping = false;
    controls.rotateSpeed = -0.3;
    controls.autoRotate = true;
    controls.autoRotateSpeed = -1;
    controls.keyPanSpeed = 4;
    //controls.minZoom = 0.75;
    //controls.maxZoom = 2.8;

    window.addEventListener("resize", onWindowResize, false); 
    //buttonrender.addEventListener("click",()=> {controls.autoRotate = false}, false);
    init();
    animate();
    onChangePosition(currentSceneId);
    main.onwheel = onScrollCameraStateChange;
  }, []);
  const onClickBegin = () => {
    var overlay = document.getElementById("overlay");
    overlay.remove();
  };

  const getSceneById = (sceneId) => {
    console.log("current sceneId", sceneId);
    return scenes.find((scene) => scene.id === sceneId);
  };
  const handleDoorClick = async (sceneToGo) => {
    setCurrentSceneId(sceneToGo.id);
    renderBackGround(sceneToGo.image);
    renderDoors(sceneToGo.id);
    renderHints(sceneToGo.id);
    onChangePosition(sceneToGo.id);
    camera.fov = CAMERA_DEFAULT_FOV;
    controls.autoRotate = true;

  };

  const renderBackGround = (imgSrc) => {
    var textureNext = new THREE.TextureLoader().load(require(imgSrc + ".jpg"));
    textureNext.minFilter = THREE.LinearFilter;
    textureNext.magFilter = THREE.LinearFilter;
    const material = new THREE.MeshBasicMaterial({
      map: textureNext,
    });
    mesh1 = new THREE.Mesh(geometry, material);
    scene.add(mesh1);
  };

  const renderDoors = (sceneIn) => {
    const currentScene = scenes.find((scene) => scene.id === sceneIn);
    currentScene.doors.map((door, i) => {
      const sceneToGo = getSceneById(door.sceneId);
      var elementCreate = document.createElement("div");
      var element = addButton(elementCreate, door.sceneId, sceneToGo.preview);
      ["click", "touchstart"].forEach((evt) =>
        element.addEventListener(evt, function () {
          // for (var i = 0.7; i > 0.1; i -= 0.1) {
          //     camera.fov = i * 2 * CAMERA_DEFAULT_Z;
          //     console.log("zoomIn");
          //   setTimeout(camera.updateProjectionMatrix(), 10000);
          // }

          currentScene.doors.map((door, i) => {
            var selectedObject = scene.getObjectByName(door.sceneId);
            scene.remove(selectedObject);
          });
          currentScene.hints.map((hint, i) => {
            var selectedObject = scene.getObjectByName(hint.description);
            scene.remove(selectedObject);
          });
          handleDoorClick(sceneToGo);
          audio.play();
        })
      );
      var domObject = new CSS3DObject(element);
      domObject.position.set(
        door.location.posX,
        door.location.posY,
        door.location.posZ
      );
      domObject.rotateY(1.7 + door.location.rotate);
      domObject.name = door.sceneId;
      scene.add(domObject);
      console.log("object name", domObject.name);
    });
  };

  const renderHints = (sceneIn) => {
    const currentScene = scenes.find((scene) => scene.id === sceneIn);
    currentScene.hints.map((hint, i) => {
      console.log(hint);
      var elementCreate = document.createElement("div");
      var element = addHints(elementCreate, hint.description);
      var domObject = new CSS3DObject(element);
      domObject.position.set(
        hint.location.posX,
        hint.location.posY,
        hint.location.posZ
      );
      domObject.rotateY(1.7 + hint.location.rotate);
      domObject.name = hint.description;
      scene.add(domObject);
      console.log("object name", domObject.name);
    });
  };

  const onScrollCameraStateChange = (event) => {
    console.log("event", event);
    // Restrict scale
    scale += event.deltaY * 0.0001;

    scale = Math.min(Math.max(0.1, scale), 0.6);

    console.log("scale", scale);
    camera.fov = scale * 2 * CAMERA_DEFAULT_FOV;
    camera.updateProjectionMatrix();
  };

  const onClickCameraStateChange = (value) => {
    console.log(value);
    camera.fov = value * 2 * CAMERA_DEFAULT_FOV;
    camera.updateProjectionMatrix();
  };
  function init() {
    var texture = new THREE.TextureLoader().load(
      require("./img/panos/hallA.jpg")
    );
    texture.minFilter = THREE.LinearFilter;
    texture.magFilter = THREE.LinearFilter;
    console.log(texture);
    const material = new THREE.MeshBasicMaterial({
      map: texture,
    });
    mesh = new THREE.Mesh(geometry, material);
    scene.add(mesh);

    const currentScene = scenes.find((scene) => scene.id === currentSceneId);
    console.log(currentScene);
    renderDoors(currentScene.id);
    renderHints(currentScene.id);
  }

  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
  }
  function onDocumentMouseMove(event) {
    mouseX = event.clientX - windowHalfX;
    mouseY = event.clientY - windowHalfY;
  }

  function animate() {
    scene.updateMatrixWorld();
    requestAnimationFrame(animate);
    controls.update();

    renderer.render(scene, camera);
    renderer2.render(scene, camera);
  }

  // function update()
  // {
  //   if ( keyboard.pressed("p") )
  //     video.play();

  //   if ( keyboard.pressed("space") )
  //     video.pause();

  //   if ( keyboard.pressed("s") ) // stop video
  //   {
  //     video.pause();
  //     video.currentTime = 0;
  //   }

  //   if ( keyboard.pressed("r") ) // rewind video
  //     video.currentTime = 0;

  //   controls.update();
  //   stats.update();
  // }

  function addHints(element, textName) {
    element.style.width = "80px";
    element.style.height = "20px";
    element.style.border = "1px solid orange";
    element.style.borderRadius = "3px";
    element.style.opacity = 0.99;
    element.textContent = textName;
    element.style.color = "red";

    element.style.margin = "auto";
    element.style.fontSize = "10px";
    element.style.transition = "all 0.5s";
    element.addEventListener("mouseover", () => onMouseOver(element));
    element.addEventListener("mouseout", () => onMouseOut(element));

    return element;
  }
  function addButton(element, textName, url) {
    element.style.width = "50px";
    element.style.height = "20px";
    element.style.border = "1px solid orange";
    element.style.borderRadius = "3px";
    element.style.opacity = 0.99;
    element.style.margin = "auto";
    element.textContent = textName;
    element.style.color = "red";

    element.style.fontSize = "10px";
    element.style.backgroundColor = "orange";
    element.style.transition = "all 0.5s";

    element.style.backgroundImage = "url(" + require(url + ".jpg") + ")";
    element.style.backgroundSize = "50px 20px";
    element.style.transition = "all 0.5s";
    element.addEventListener("mouseover", () => onMouseOver(element));
    element.addEventListener("mouseout", () => onMouseOut(element));
    return element;
  }
  function onMouseOver(element) {
    element.style.width = 1.3 * 50 + "px";
    element.style.backgroundSize = 1.3 * 50 + "px 20px";
  }
  function onMouseOut(element) {
    element.style.width = 50 + "px";
    element.style.backgroundSize = 50 + "px 20px";
  }
  return (
    <div id="main">
      <div
        id="overlay"
        style={{
          position: "absolute",
          zIndex: 1100,
          width: window.innerWidth - 20,
          height: window.innerHeight - 20,
          backgroundColor: "gray",
          opacity: 0.8,
          justifyContent: "center",
        }}
      >
        <Button
          style={{
            width: 240,
            height: 200,
            background: "url(" + require("./img/click2play.png") + ")",
            backgroundSize: "240px 200px",
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            opacity: 1,
          }}
          id="startButton"
          onClick={onClickBegin}
        ></Button>
      </div>
      {/* //<ImageGallery scenes={scenes}></ImageGallery> */}
      <div id="homeBanner"></div>
      <div id="css"></div>
      <div
        style={{
          position: "absolute",
          zIndex: 1020,
          bottom: 10,
          right: 10,
          opacity: 0.8,
        }}
      >
        Zoom{" "}
        <input
          type="range"
          value="0.35"
          min="0.1"
          max="0.7"
          step="0.001"
          id="input"
          onChange={(event) => onClickCameraStateChange(event.target.value)}
        ></input>
      </div>
      <div
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          zIndex: 1000,
          backgroundColor: "#fffee6",
          border: "2px solid black",
          opacity: 0.8,
        }}
      >
        <img
          style={{
            width: window.innerWidth < 1000 ? 150 : 300,
            height: window.innerHeight < 700 ? 150 : 300,
          }}
          src={require("./img/Bind_mini-map.png")}
        ></img>
        <div
          style={{
            position: "absolute",
            top: 84,
            right: 70,
            zIndex: 1003,
            overflow: "visible",
            opacity: 1,
            cursor: "pointer",
            pointerEvents: "auto",
            background:
              displayInA === "On"
                ? "url(" +
                  require("./img/map_point.png") +
                  ")" +
                  "24.4px 24.4px"
                : "url(" +
                  require("./img/map_point_disable.png") +
                  ")" +
                  "24.4px 24.4px",
            border: "solid 1px rgb(0, 0, 0)",
            borderRadius: "50px",
            width: 24.8,
            height: 24.8,
          }}
        >
          {displayInA == "On" && (
            <div
              style={{
                position: "absolute",
                zIndex: 1001,
                overflow: "visible",
                opacity: 1,
                cursor: "pointer",
                pointerEvents: "none",
                background: "none",
                width: 64,
                height: 64,
                transform:
                  "translateZ(1e+12px) translate(-20px, -20px) translate(0px, 0px) rotate(0deg) translate(0px, 0px)",
              }}
            >
              <svg
                width={66}
                height={66}
                style={{ position: "absolute", left: -1, top: -1 }}
              >
                <path
                  stroke="rgb(255,255,255)"
                  strokeWidth="0"
                  strokeOpacity="0.3"
                  fillOpacity="0.5"
                  d="M 32.5,32.5 L 3.353298478515189,19.291298685431826 A 32,32 0 0 1 50.89934788624416,6.318632630037012 Z"
                  style={{
                    pointerEvents: "visiblepainted",
                    cursor: "pointer",
                    transform: "scale(1, 1)",
                  }}
                ></path>
              </svg>
            </div>
          )}
        </div>
        <div
          style={{
            position: "absolute",
            top: 84,
            right: 145,
            zIndex: 1003,
            overflow: "visible",
            opacity: 1,
            cursor: "pointer",
            pointerEvents: "auto",
            background:
              displayInB === "On"
                ? "url(" +
                  require("./img/map_point.png") +
                  ")" +
                  "24.4px 24.4px"
                : "url(" +
                  require("./img/map_point_disable.png") +
                  ")" +
                  "24.4px 24.4px",
            border: "solid 1px rgb(0, 0, 0)",
            borderRadius: "50px",
            width: 24.8,
            height: 24.8,
          }}
        >
          {displayInB == "On" && (
            <div
              style={{
                position: "absolute",
                zIndex: 1001,
                overflow: "visible",
                opacity: 1,
                cursor: "pointer",
                pointerEvents: "none",
                background: "none",
                width: 64,
                height: 64,
                transform:
                  "translateZ(1e+12px) translate(-20px, -20px) translate(0px, 0px) rotate(0deg) translate(0px, 0px)",
              }}
            >
              <svg
                width={66}
                height={66}
                style={{ position: "absolute", left: -1, top: -1 }}
              >
                <path
                  stroke="rgb(255,255,255)"
                  strokeWidth="0"
                  strokeOpacity="0.3"
                  fillOpacity="0.5"
                  d="M 32.5,32.5 L 3.353298478515189,19.291298685431826 A 32,32 0 0 1 50.89934788624416,6.318632630037012 Z"
                  style={{
                    pointerEvents: "visiblepainted",
                    cursor: "pointer",
                    transform: "scale(1, 1)",
                  }}
                ></path>
              </svg>
            </div>
          )}
        </div>
        <div
          style={{
            position: "absolute",
            top: 72,
            left: 73,
            zIndex: 1003,
            overflow: "visible",
            opacity: 1,
            cursor: "pointer",
            pointerEvents: "auto",
            background:
              displayInC === "On"
                ? "url(" +
                  require("./img/map_point.png") +
                  ")" +
                  "24.4px 24.4px"
                : "url(" +
                  require("./img/map_point_disable.png") +
                  ")" +
                  "24.4px 24.4px",
            border: "solid 1px rgb(0, 0, 0)",
            borderRadius: "50px",
            width: 24.8,
            height: 24.8,
          }}
        >
          {displayInC == "On" && (
            <div
              style={{
                position: "absolute",
                zIndex: 1001,
                overflow: "visible",
                opacity: 1,
                cursor: "pointer",
                pointerEvents: "none",
                background: "none",
                width: 64,
                height: 64,
                transform:
                  "translateZ(1e+12px) translate(-20px, -20px) translate(0px, 0px) rotate(0deg) translate(0px, 0px)",
              }}
            >
              <svg
                width={66}
                height={66}
                style={{ position: "absolute", left: -1, top: -1 }}
              >
                <path
                  stroke="rgb(255,255,255)"
                  strokeWidth="0"
                  strokeOpacity="0.3"
                  fillOpacity="0.5"
                  d="M 32.5,32.5 L 3.353298478515189,19.291298685431826 A 32,32 0 0 1 50.89934788624416,6.318632630037012 Z"
                  style={{
                    pointerEvents: "visiblepainted",
                    cursor: "pointer",
                    transform: "scale(1, 1)",
                  }}
                ></path>
              </svg>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

// const useStyles = makeStyles((theme) => ({
//   root: {
//     width: "100%",
//     height: "100vh",
//   },
//   form: {
//     [theme.breakpoints.down("sm")]: {
//       width: "100%",
//     },
//     [theme.breakpoints.up("md")]: {
//       width: "78%",
//     },
//     [theme.breakpoints.up("lg")]: {
//       width: "64%",
//     },
//     height: "80vh",
//     margin: "auto",
//   },
// }));
export default SceneManager;
