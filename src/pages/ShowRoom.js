import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import {
  CSS3DObject,
  CSS3DRenderer,
} from "three/examples/jsm/renderers/CSS3DRenderer";
import React, { useEffect, useRef, useState } from "react";
var camera,
  element,
  scene,
  renderer,
  renderer2,
  windowHalfX,
  windowHalfY,
  background;
var controls;
var mouseX, mouseY;
var video, videoImage, videoImageContext, videoTexture;
var audio = new Audio(require("./media/menu-click.wav"));

export default function ShowRoom({ onBack, onClickTour }) {
  useEffect(() => {
    init();
    animate();
  }, []);

  function init() {
    var container, buttonrender, mesh;
    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    container = document.getElementById("homeBanner");
    buttonrender = document.getElementById("css");

    camera = new THREE.PerspectiveCamera(
      45,
      window.innerWidth / window.innerHeight,
      1,
      1100
    );
    camera.position.set(45, 1, 1);
    scene = new THREE.Scene();

    var geometry = new THREE.SphereBufferGeometry(200, 500, 1000);
    // invert the geometry on the x-axis so that all of the faces point inward
    geometry.scale(-1, 1, 1);

    var loader = new THREE.TextureLoader();
    const material1 = new THREE.MeshBasicMaterial({
      map: loader.load(require("./img/thechester.jpg")),
    });
    mesh = new THREE.Mesh(geometry, material1);

    scene.add(mesh);

    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);

    //Render 3D button
    addBanner(0, 60, 0, -0.1, require("./img/toto.png"));
    function addButtonA() {
      var elementCreate = document.createElement("div");
      var element = addButton(elementCreate, "Product", "A");
      //element.addEventListener("click", () => onClick("A"))
      var domObject = new CSS3DObject(element);
      domObject.position.set(-200, 20, 80);
      domObject.rotateY(1.7 + 0.2);
      scene.add(domObject);
    }
    addButtonA();

    function addButtonB() {
      var elementCreate = document.createElement("div");
      var element = addButton(elementCreate, "Product", "B");
      // element.addEventListener("click", () => onClick("B"))
      var domObject = new CSS3DObject(element);
      domObject.position.set(-200, -20, 80);
      domObject.rotateY(1.7 + 0.2);
      scene.add(domObject);
    }
    addButtonB();

    function addButtonC() {
      var elementCreate = document.createElement("div");
      var element = addButton(elementCreate, "Product", "SYNLA");
      //element.addEventListener("click", () => onClick("C"))
      var domObject = new CSS3DObject(element);
      domObject.position.set(-200, 20, -80);
      domObject.rotateY(1.7 - 0.4);
      scene.add(domObject);
    }
    addButtonC();

    function addButtonD() {
      var elementCreate = document.createElement("div");
      var element = addButton(elementCreate, "Video", "___");
      ["click", "touchstart"].forEach((evt) =>
        element.addEventListener(evt, function () {
          handlePlayVideo();
          audio.play();
        })
      );
      var domObject = new CSS3DObject(element);
      domObject.position.set(-200, -20, -80);
      domObject.rotateY(1.7 - 0.4);
      scene.add(domObject);
    }
    addButtonD();

    function addButtonE() {
      var elementCreate = document.createElement("div");
      var element = addButton(elementCreate, "Back", "");
      ["click", "touchstart"].forEach((evt) =>
        element.addEventListener(evt, function () {
          onBack();
          console.log("Start call");
          audio.play();
        })
      );

      var domObject = new CSS3DObject(element);
      domObject.position.set(-200, -60, 80);
      domObject.rotateY(1.7 + 0.2);
      scene.add(domObject);
    }
    addButtonE();

    function addButtonF() {
      var elementCreate = document.createElement("div");
      element = addButton(elementCreate, "Visit", "", 0, -60, -80, -0.4);
      ["click", "touchstart"].forEach((evt) =>
        element.addEventListener(evt, function () {
          onClickTour();
          audio.play();
          //video.play();
          console.log("play");
        })
      );
      var domObject = new CSS3DObject(element);
      domObject.position.set(-200, -60, -80);
      domObject.rotateY(1.7 - 0.4);
      scene.add(domObject);
    }
    addButtonF();

    renderer2 = new CSS3DRenderer();
    renderer2.setSize(window.innerWidth, window.innerHeight);
    renderer2.domElement.style.position = "absolute";
    renderer2.domElement.style.top = 0;
    buttonrender.appendChild(renderer2.domElement);

    document.addEventListener("mousemove", onDocumentMouseMove, false);
    controls = new OrbitControls(camera, renderer2.domElement);
    controls.enableZoom = true;
    controls.enablePan = false;
    controls.enableDamping = true;
    controls.rotateSpeed = -0.1;

    window.addEventListener("resize", onWindowResize, false);
  }
  const handlePlayVideo = () => {
    console.log("play");

    video = document.getElementById("video");
    video.play();
    //video.autoplay; // must call after setting/changing source

    videoImage = document.createElement("canvas");
    videoImage.width = 480;
    videoImage.height = 204;

    videoImageContext = videoImage.getContext("2d");
    // background color if no video present
    videoImageContext.fillStyle = "#000000";
    videoImageContext.fillRect(0, 0, videoImage.width, videoImage.height);

    videoTexture = new THREE.Texture(videoImage);
    videoTexture.minFilter = THREE.LinearFilter;
    videoTexture.magFilter = THREE.LinearFilter;

    var movieMaterial = new THREE.MeshBasicMaterial({
      map: videoTexture,
      overdraw: true,
      side: THREE.DoubleSide,
    });
    // the geometry on which the movie will be displayed;
    // 		movie image will be scaled to fit these dimensions.
    var movieGeometry = new THREE.PlaneGeometry(10, 70, 4, 4);
    var movieScreen = new THREE.Mesh(movieGeometry, movieMaterial);
    movieScreen.position.set(100, 0,0);
    movieScreen.rotateY(-1.5);
    //movieScreen.rotateY(1);
    scene.add(movieScreen);
    video.play();

    // camera.position.set(0, 150, 300);
    //camera.lookAt(movieScreen.position);
  };

  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
  }
  function onDocumentMouseMove(event) {
    mouseX = event.clientX - windowHalfX;
    mouseY = event.clientY - windowHalfY;
  }

  function animate(time) {
    scene.updateMatrixWorld();
    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
    renderer2.render(scene, camera);
  }

  function addBanner(x, y, z, rotate, url) {
    var element = document.createElement("div");
    element.style.width = "120px";
    element.style.height = "30px";
    element.style.opacity = 0.99;
    element.style.backgroundImage = "url(" + url + ")";
    element.style.backgroundSize = "120px 30px";
    var domObject = new CSS3DObject(element);
    domObject.position.set(-200, y, z);
    domObject.rotateY(1.7 + rotate);
    scene.add(domObject);
  }

  function addButton(element, textType, textName) {
    element.style.width = "60px";
    element.style.height = "25px";
    element.style.border = "1px solid orange";
    element.style.borderRadius = "3px";
    element.style.opacity = 0.99;
    element.textContent = textType + "\r\n" + textName;
    element.style.margin = "auto";
    element.style.fontSize = "10px";
    element.style.backgroundColor = "orange";
    element.style.transition = "all 0.5s";
    element.addEventListener("mouseover", () => onMouseOver(element));
    element.addEventListener("mouseout", () => onMouseOut(element));

    return element;
  }

  function onMouseOver(element) {
    element.style.width = 1.3 * 60 + "px";
    element.style.backgroundSize = 1.3 * 60 + "px 25px";
  }
  function onMouseOut(element) {
    element.style.width = 60 + "px";
    element.style.backgroundSize = 60 + "px 20px";
  }

  return (
    <div>
      <div id="homeBanner"></div>
      <div id="css"></div>
      <video
        src="https://media.giphy.com/media/XyIveZZnnuNwksEkKm/source.mp4"
        id="video"
        crossOrigin="anonymous"
        style={{ display: "none" }}
      ></video>
    </div>
  );
}
