import * as THREE from 'three';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { CSS3DObject, CSS3DRenderer } from "three/examples/jsm/renderers/CSS3DRenderer";
import React, { useEffect } from 'react';
import ShowRoom from './ShowRoom';
var camera, element, scene, renderer, renderer2, windowHalfX, windowHalfY, background;
var controls;
var mouseX, mouseY;
var audio = new Audio(require('./media/menu-click.wav'));

export default function Home({onClick}){
    useEffect(() => {
        init();
        animate();
        },[] );
    
    const buttonOnClick =(text)=> {
        onClick(text);
        console.log(" Start");
        audio.play();
    };
    function init() {

        var container, buttonrender, mesh;
        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;

        container = document.getElementById( 'homeBanner' );
        buttonrender = document.getElementById( 'css' );

        camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 300 );
        camera.position.set( 50, 0, 0 );
        scene = new THREE.Scene();

        var geometry = new THREE.SphereBufferGeometry( 200, 500, 1000 );
        // invert the geometry on the x-axis so that all of the faces point inward
        geometry.scale( -1, 1, 1 );

        var loader = new THREE.TextureLoader();
        const materialBackground = new THREE.MeshBasicMaterial({
            map: loader.load(require('../assets/static_assets/exhibition.jpg')),
          });
        mesh = new THREE.Mesh( geometry, materialBackground );
        scene.add( mesh );

        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );
        container.appendChild( renderer.domElement );
        

        function addButtonA() {
            var elementCreate = document.createElement("div");
            var element = addButton(elementCreate, 'A', require('./img/hitachi.png'));
            ['click','touchstart'].forEach( evt => 
              element.addEventListener(evt, function () {
                buttonOnClick("A");
                scene.remove( domObject );
              })
          );        
            var domObject = new CSS3DObject(element);
            domObject.position.set(-30, 0, 200);
            domObject.rotateY(3);
            scene.add(domObject);
          };
          addButtonA();
          function addButtonB() {
            var elementCreate = document.createElement("div");
            var element = addButton(elementCreate, 'B', require('./img/panasonic.png'));
            ['click','touchstart'].forEach( evt => 
              element.addEventListener(evt, function () {
                buttonOnClick("A");
                scene.remove( domObject );
              })
          );
            
            var domObject = new CSS3DObject(element);
            domObject.position.set(45, 0, 200);
            domObject.rotateY(3.33);
            scene.add(domObject);
          };
          addButtonB();
          function addButtonC() {
            var elementCreate = document.createElement("div");
            var element = addButton(elementCreate, 'C', require('./img/toto.png')  );
            ['click','touchstart'].forEach( evt => 
              element.addEventListener(evt, function () {
                buttonOnClick("A");
                scene.remove( domObject );
              })
          );
            
            var domObject = new CSS3DObject(element);
            domObject.position.set(45, 0, -200);
            domObject.rotateY(-0.1);
            scene.add(domObject);
          };
          addButtonC();
          function addButtonD() {
            var elementCreate = document.createElement("div");
            var element = addButton(elementCreate, 'D', require('./img/sekisui.png'));
            ['click','touchstart'].forEach( evt => 
              element.addEventListener(evt, function () {
                buttonOnClick("A");
                scene.remove( domObject );
              })
          );
            
            var domObject = new CSS3DObject(element);
            domObject.position.set(-30, 0, -200);
            domObject.rotateY(0.2);
            scene.add(domObject);
          };
          addButtonD();


        renderer2 = new CSS3DRenderer();
        renderer2.setSize( window.innerWidth, window.innerHeight );
        renderer2.domElement.style.position = 'absolute';
        renderer2.domElement.style.top = 0;
        buttonrender.appendChild( renderer2.domElement );

        document.addEventListener( 'mousemove', onDocumentMouseMove, false );
        controls = new OrbitControls( camera, renderer2.domElement );
        controls.enableZoom = true;
        controls.enablePan = true;
        controls.enableDamping = true;
        controls.rotateSpeed = -0.1;

        window.addEventListener( 'resize', onWindowResize, false );

    }
    function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize( window.innerWidth, window.innerHeight );

    }
    function onDocumentMouseMove( event ) {
        mouseX = event.clientX - windowHalfX;
        mouseY = event.clientY - windowHalfY;
    }

    function animate() {
        scene.updateMatrixWorld()
        requestAnimationFrame( animate );
        controls.update();
        renderer.render( scene, camera );
        renderer2.render( scene, camera );
    }

    function addButton(element, textContent, url ){
        element.style.width = '50px';
        element.style.height = '20px';
        element.style.border = '1px solid orange';
        element.style.borderRadius = "3px";                      
        element.style.opacity = 0.99;

        
        element.style.backgroundImage = 'url('+ url +')';
        element.style.backgroundSize = "50px 20px";
        element.style.transition = "all 0.5s";
        element.addEventListener('mouseover',() => onMouseOver(element));
        element.addEventListener('mouseout',() => onMouseOut(element));
        return element;
    }
    function onMouseOver(element){
        element.style.width = 1.3 * 50 +'px';
        element.style.backgroundSize = 1.3*50 +"px 20px";
    }
    function onMouseOut(element){
        element.style.width = 50 +'px';
        element.style.backgroundSize = 50 +"px 20px";
    }
    return (

        <div>
            <div style = {{height: window.innerHeight, width: window.innerWidth}} id = "homeBanner"></div>
            <div style = {{height: window.innerHeight, width: window.innerWidth}} id="css"></div>  
        </div>
    );
}

			