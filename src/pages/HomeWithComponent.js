import * as THREE from 'three';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { CSS3DObject, CSS3DRenderer } from "three/examples/jsm/renderers/CSS3DRenderer";
import React, { useEffect } from 'react';
import ShowRoom from './ShowRoom';
import ButtonThree from "../components/ButtonThree";

var camera, element, renderer, renderer2, windowHalfX, windowHalfY, background;
var controls;
var mouseX, mouseY;
var audio = new Audio(require('./media/menu-click.wav'));
var scene = new THREE.Scene();

export default function Home({ onClick }) {
    useEffect(() => {
        init();
        animate();
    }, []);
    const onRenderButton = (domObject) => {
        console.log(domObject);
        scene.add(domObject)
    }
    function init() {

        var container, buttonrender, mesh;
        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;

        container = document.getElementById('homeBanner');
        buttonrender = document.getElementById('css');

        camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1100);
        camera.position.set(120, 30, 0);
 

        var geometry = new THREE.SphereBufferGeometry(200, 500, 1000);
        // invert the geometry on the x-axis so that all of the faces point inward
        geometry.scale(-1, 1, 1);

        var loader = new THREE.TextureLoader();
        const material1 = new THREE.MeshBasicMaterial({
            map: loader.load(require('./img/chess-world.png')),
        });
        mesh = new THREE.Mesh(geometry, material1);

        scene.add(mesh);

        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight);
        container.appendChild(renderer.domElement);


        renderer2 = new CSS3DRenderer();
        renderer2.setSize(window.innerWidth, window.innerHeight);
        renderer2.domElement.style.position = 'absolute';
        renderer2.domElement.style.top = 0;
        buttonrender.appendChild(renderer2.domElement);

        document.addEventListener('mousemove', onDocumentMouseMove, false);
        controls = new OrbitControls(camera, renderer2.domElement);
        controls.enableZoom = true;
        controls.enablePan = false;
        controls.enableDamping = true;
        controls.rotateSpeed = - 0.1;

        window.addEventListener('resize', onWindowResize, false);

    }
    function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize(window.innerWidth, window.innerHeight);

    }
    function onDocumentMouseMove(event) {
        mouseX = event.clientX - windowHalfX;
        mouseY = event.clientY - windowHalfY;
    }

    function animate() {
        scene.updateMatrixWorld()
        requestAnimationFrame(animate);
        controls.update();
        renderer.render(scene, camera);
        renderer2.render(scene, camera);
    }
    return (

        <div>
            <div id="homeBanner"></div>
            <div id="css"></div>
            <ButtonThree
                title='A'
                posX="-90"
                posY="0"
                posZ="95"
                rotate="0.3"
                imgUrl={require('./img/hitachi.png')}
                onClick={() => {
                    onClick();
                }}
                onRenderButton={(domObject) => { onRenderButton(domObject) }}
            ></ButtonThree>
            <ButtonThree
                title='B'
                posX="-30"
                posY="0"
                posZ="30"
                rotate="0.1"
                imgUrl={require('./img/panasonic.png')}
                onClick={() => {
                    onClick();
                }}
                onRenderButton={(domObject) => { onRenderButton(domObject) }}
            ></ButtonThree>
            <ButtonThree
                title='C'
                posX="30"
                posY="0"
                posZ="-37"
                rotate="-0.1"
                imgUrl={require('./img/toto.png')}
                onClick={() => {
                    onClick();
                }}
                onRenderButton={(domObject) => { onRenderButton(domObject) }}
            ></ButtonThree>
            <ButtonThree
                title='D'
                posX="90"
                posY="0"
                posZ="-100"
                rotate="-0.3"
                imgUrl={require('./img/sekisui.png')}
                onClick={() => {
                    onClick();
                }}
                onRenderButton={(domObject) => { onRenderButton(domObject) }}
            ></ButtonThree>
        </div>
    );
}

